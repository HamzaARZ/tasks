import java.util.Scanner;

public class Menu {
    private String allOptions;

    public Menu(String allOptions) {
        this.allOptions = allOptions;
    }

    public void showMenu(){
        String[] options = allOptions.split(" ");
        for (int i = 0; i < options.length; i++)
            System.out.println(i+1 + " : " + options[i]);
    }

    public int getOptin(){
        Scanner keyBoard = new Scanner(System.in);
        while (true){
            this.showMenu();
            System.out.print("set your option : ");
            int option = keyBoard.nextInt();
            if(0 < option && option <= allOptions.split(" ").length)
                return option;
            else
                System.out.println("invalid input !!");
        }
    }

    /*//if option == 1 :
    public static .... .....(){
            .....
    }
    //if option == 2 ......
    ....
    */
}