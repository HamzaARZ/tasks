
public interface DataAccessInterface <T>{

    public void add(T element);
    public void delete(int id);
    public void update(int id, T newElement);
}
