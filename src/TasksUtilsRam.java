import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.stream.Collectors;

public class TasksUtilsRam implements DataAccessInterface<Task> {

    private HashMap<Integer, Task> tasks;

    public TasksUtilsRam(){
        this.tasks = new HashMap<Integer, Task>();
    }


    public HashMap<Integer, Task> getTasks() {return tasks;}
    public void setTasks(HashMap<Integer, Task> tasks) {this.tasks = tasks;}



    @Override
    public void add(Task task){
        //task.detPriority can t be null
        if(task != null && task.getPriority() != null)
            this.tasks.put(task.getId(), task);
        else
            System.out.println("Can't add null Task !!");
    }

    @Override
    public void delete(int id){
        tasks.remove(id);
    }

    @Override
    public void update(int id, Task newTask){
        //task.detPriority can t be null
        if(newTask != null && newTask.getPriority() != null){
            Task taskToUpdate = tasks.get(id);
            //taskToUpdate = null ????
            taskToUpdate.setDescription(newTask.getDescription());
            taskToUpdate.setDate(newTask.getDate());
            taskToUpdate.setPriority(newTask.getPriority());

        }
        else
            System.out.println("invalid id or priority !!");

    }

    @Override
    public String toString() {
        String description = "\t" + this.tasks.values().stream().map((Task task) -> task.toString()).collect(Collectors.joining("\n\t"));
        return "Tasks{\n" + description + "\n\t}";
    }
}