import java.time.LocalDateTime;
import java.util.Arrays;

public class Task {

    public static int nextIdToAdd = 1;

    private int id;
    private String description;
    private LocalDateTime date;
    private String priority;


    public Task(String description, LocalDateTime date, String priority) {

        //verification of value of priority (Lowest, Low, Medium, High, Highest)
        if(Arrays.asList("LOWEST", "LOW", "MEDIUM", "HIGH", "HIGHEST").contains(priority.toUpperCase())){
            this.id = nextIdToAdd;
            this.description = description;
            this.date = date;
            this.priority = priority.toUpperCase();

            nextIdToAdd++;
        }
        else
            System.out.println("invalid priority !!");
    }


    public int getId() {return id;}
    public String getDescription() {return description;}
    public LocalDateTime getDate() {return date;}
    public String getPriority() {return priority;}


    public void setDescription(String description) {this.description = description;}
    public void setDate(LocalDateTime date) {this.date = date;}
    public void setPriority(String priority) {
        if(Arrays.asList("LOWEST", "LOW", "MEDIUM", "HIGH", "HIGHEST").contains(priority.toUpperCase()))
            this.priority = priority.toUpperCase();
        else
            System.out.println("invalid priority !!");
    }

    @Override
    public String toString() {
        return "Task "+ id +" : description : " + description + "\tdate : " + date + "\tpriority : " + priority ;
    }
}
